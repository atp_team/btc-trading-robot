package ru.atp_team;

import ru.atp_team.libs.Props;
import ru.atp_team.libs.btce.api.TradeApi;

/**
 * Created by sbt-velichko-aa on 20.09.2016.
 */
public class WorkWithBtce {

    private TradeApi tradeApi;
    private String key = Props.get("user.key");
    private String secret = Props.get("user.secret");


    /**
     * Get last price for BTC-USD and LTC-USD
     */
    public void getLastPrice() {
        tradeApi = new TradeApi();
        tradeApi.ticker.addPair("btc_usd"); // add btc_usd pair to ticker method
        tradeApi.ticker.addPair("ltc_usd");
        tradeApi.ticker.runMethod();// run ticker API method with added parametres
        while (tradeApi.ticker.hasNextPair()) {
            tradeApi.ticker.switchNextPair();// in this case, switch to first pairs, i.e. btc_usd, for the next iteration it will be ltc_usd
            System.out.println(tradeApi.ticker.getCurrentLast()); // print last price for current pair
        }
    }

    /**
     * Get active orders for BTC-USD (depth API method)
     * this is current glass
     */
    public void getActiveOrders() {
        tradeApi = new TradeApi();
        tradeApi.depth.addPair("btc_usd");
        tradeApi.depth.setLimit(1000);
        tradeApi.depth.runMethod();
        tradeApi.depth.setCurrentPair("btc_usd");// now instead switchNextPair we can use this method, but switchNextPair works too
        System.out.println("print asks");
        while (tradeApi.depth.hasNextAsk()) {
            tradeApi.depth.switchNextAsk();// switch to next ask. In this case we can switch pairs and can switch asks and bids for each pair
            System.out.printf("Price %s, amount: %s.\n", tradeApi.depth.getCurrentAskPrice(), tradeApi.depth.getCurrentAskAmount());
        }
        System.out.println("----end asks----");
        System.out.println("print bids");
        while (tradeApi.depth.hasNextBid()) {
            tradeApi.depth.switchNextBid();
            System.out.printf("Price %s, amount: %s.\n", tradeApi.depth.getCurrentBidPrice(), tradeApi.depth.getCurrentBidAmount());
        }
    }

    /**
     * Get balance
     */
    public void getBalance() throws Exception {
        tradeApi = new TradeApi(key, secret);// we can use constructor, we can use empty constructor and after that t.setKeys(...) and so on.
        tradeApi.getInfo.runMethod();
        System.out.println(tradeApi.getInfo.getBalance("usd")); // get USD balance. We can get currency list with getCurrencyList()
    }

    /**
     * Main method to create order to but or sell
     *
     * @param pair   ex. btc_usd
     * @param type   - buy or sell
     * @param rate   - course what need to buy/sell. ex.Например используя rate=0.1 при продаже можно продать по самой выгодней цене по рынку.
     * @param amount - count what need to buy/sell.
     * @throws Exception
     */
    public void createOrder(String pair, String type, String rate, String amount) throws Exception {
        tradeApi = new TradeApi(key, secret);
        tradeApi.trade.setPair(pair);
        tradeApi.trade.setType(type);
        tradeApi.trade.setRate(rate);
        tradeApi.trade.setAmount(amount);
        tradeApi.trade.runMethod();
        System.out.printf("Валюты куплено/продано: %s. \n " +
                        "Сколько валюты осталось купить/продать (и на сколько был создан ордер): %s. \n" +
                        "order_id: %s (Имеет значение 0 если запрос был полностью удовлетворен встречными ордерами).\n",
                tradeApi.trade.getReceived(), tradeApi.trade.getRemains(), tradeApi.trade.getOrder_id());
    }

    /**
     * Get our active orders
     *
     * @param pair
     * @throws Exception
     */
    public void getOurActiveOrders(String pair) throws Exception {
        tradeApi = new TradeApi(key, secret);
        tradeApi.activeOrders.setPair(pair);
        tradeApi.activeOrders.runMethod();
        System.out.printf("pair: %s. \n " +
                        "type: %s. \n" +
                        "amount: %s.\n" +
                        "rate: %s.\n" +
                        "timestamp_created: %s.\n" +
                        "status: %s.\n",
                tradeApi.activeOrders.getCurrentPair(), tradeApi.activeOrders.getCurrentType(),
                tradeApi.activeOrders.getCurrentAmount(), tradeApi.activeOrders.getCurrentRate(),
                tradeApi.activeOrders.getCurrentTimestamp_created(), tradeApi.activeOrders.getCurrentStatus());
    }

    /**
     * method to cancel order
     * @param order_id
     * @throws Exception
     */
    public void cancelOrder(String order_id) throws Exception {
        tradeApi = new TradeApi(key, secret);
        tradeApi.cancelOrder.setOrder_id(order_id);
        tradeApi.cancelOrder.runMethod();
        System.out.printf("order_id: %s. \n ",
                tradeApi.cancelOrder.getOrder_id());
    }


}
