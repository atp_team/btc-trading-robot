package ru.atp_team.libs;

import ru.atp_team.libs.db.DatabaseConnectionFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by velichko-aa on 15.09.2016.
 */
public class WorkWithDB {

    public static void execute(String query, String connectionName) throws Exception {
        DatabaseConnectionFactory dbFactory = Init.getDatabaseConnectionFactory();
        Connection connection;
        if (null == query || query.isEmpty())
            throw new SQLException("Query is empty.");

        connection = dbFactory.getConnection(connectionName);
        Statement statement;
        try {
            statement = connection.createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet executeQuery(String query, String connectionName) throws Exception {
        DatabaseConnectionFactory dbFactory = Init.getDatabaseConnectionFactory();
        Connection connection;
        if (null == query || query.isEmpty())
            throw new SQLException("Query is empty.");

        connection = dbFactory.getConnection(connectionName);
        Statement statement;
        try {
            statement = connection.createStatement();
            return statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
