package ru.atp_team.libs.db;


import ru.atp_team.libs.Props;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by velichko-aa on 15.09.2016.
 */
public class DatabaseConnectionFactory {

    Map<Object, Connection> connections;

    public DatabaseConnectionFactory() {
        this.connections = new HashMap();
    }

    public Connection createConnection(String connectionName, DbType dbType, String uri, String user, String password) throws Exception {
        Connection connection;
        switch (dbType) {
            case MySQL:
                connection = getMysqlConnection(uri, user, password);
                connections.put(connectionName, connection);
                return connection;
            default:
                throw new SQLException("Unknown case type: " + dbType + ".");
        }
    }

    private Connection getMysqlConnection(String uri, String user, String password) throws Exception {
        Connection connection;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(uri, user, password);
        } catch (SQLException e) {
            throw new Exception(String.format("Don't have connection to DB. uri: %s, user: %s, password: %s.",
                    uri, user, password) + "\n" + e.getMessage());
        }

        return connection;
    }

    /**
     * get db connectino by name
     *
     * @param connectionName
     * @return connection
     */
    public Connection getConnection(String connectionName) throws Exception {
        //try to find
        for (Map.Entry<Object, Connection> connectionRow : connections.entrySet()) {
            if (connectionRow.getKey() instanceof String && connectionName.equals(connectionRow.getKey()))
                return connectionRow.getValue();
        }
        //create a new connection
        String type = Props.get(connectionName + ".type");
        String uri = Props.get(connectionName + ".uri");
        String user = Props.get(connectionName + ".user");
        String password = Props.get(connectionName + ".password");

        if (uri == null || "".equals(uri) || user == null || "".equals(user))
            throw new SQLException("Uri or login are empty.");

        return createConnection(connectionName, DbType.valueOf(type), uri, user, password);
    }
}
