package ru.atp_team.libs;


import ru.atp_team.libs.db.DatabaseConnectionFactory;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Init {

    private static DatabaseConnectionFactory dbConnectionFactory;

    public static DatabaseConnectionFactory getDatabaseConnectionFactory() {
        if (null == dbConnectionFactory) {
            dbConnectionFactory = new DatabaseConnectionFactory();
        }
        return dbConnectionFactory;
    }
}
