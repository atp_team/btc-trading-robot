package ru.atp_team.indicators.entry;

import ru.atp_team.libs.WorkWithDB;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sbt-velichko-aa on 27.09.2016.
 */
public class Candle {

    private String date;
    private float min;
    private float close;
    private float open;
    private float max;
    private float tender;
    private float differentMin;
    private float differentMax;

    /**
     * Init from params
     * @param date
     * @param min
     * @param close
     * @param open
     * @param max
     * @param tender
     */
    public Candle(String date, float min, float close, float open, float max, float tender) {
        this.date = date;
        this.min = min;
        this.close = close;
        this.open = open;
        this.max = max;
        this.tender = tender;
        //определяем свечу
        if (open < close) {
            //свеча бычья
            differentMax = max - close;
            differentMin = open - min;
        } else if (open > close) {
            //свеча медвежья
            differentMax = max - open;
            differentMin = close - min;
        } else {
            differentMax = 0;
            differentMin = 0;
        }
    }

    /**
     * Init from db
     * @param candle
     * @throws SQLException
     */
    public Candle (ResultSet candle) throws SQLException {
        this.date = candle.getString("date");
        this.min = candle.getFloat("min");
        this.close = candle.getFloat("close");
        this.open = candle.getFloat("open");
        this.max = candle.getFloat("max");
        this.tender = candle.getFloat("tender");
        this.differentMax = candle.getFloat("differentMax");
        this.differentMin = candle.getFloat("differentMin");
    }

    public float getTender() {
        return tender;
    }

    public float getDifferentMin() {
        return differentMin;
    }

    public float getDifferentMax() {
        return differentMax;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "[date: \"" + date + "\", min: \"" + min + "\", close: \"" + close + "\", open: \"" + open +
                "\", max: \"" + max + "\", tender: \"" + tender + "\", differentMin: \"" + differentMin + "\"," +
                " differentMax: \"" + differentMax + "\"]";
    }

    public void saveToDb() throws Exception {
        //check that row with this time isn't exist
        String queryCheck = "SELECT count(*) FROM sys.btce_table where date = '" + date + "%'";
        ResultSet rs = WorkWithDB.executeQuery(queryCheck, "db");
        rs.next();
        if (rs.getInt(1) == 0) {
            System.out.println("write to db candle with data: " + toString());
            String query = String.format("INSERT INTO `sys`.`btce_table` (`date`, `min`, `close`, `open`, `max`, `tender`," +
                            " `differentMin`, `differentMax`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');",
                    date, min, close, open, max, tender, differentMin, differentMax);
            WorkWithDB.execute(query, "db");
        }
    }
}