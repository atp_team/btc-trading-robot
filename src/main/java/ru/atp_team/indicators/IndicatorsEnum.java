package ru.atp_team.indicators;

/**
 * Created by nestor on 22.09.2016.
 */
public enum IndicatorsEnum {
    Up,
    Down,
    TerraIncognito,
    MbUp,
    MbDown

}
