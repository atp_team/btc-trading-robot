package ru.atp_team.indicators;

/**
 * Created by nestor on 22.09.2016.
 */
public class BtceTableIndicators {

    public static IndicatorsEnum predicateByShadow(float difMax, float difMin) {
        IndicatorsEnum result;
        boolean up = false, down = false;

        //смотрим какая тень больше
        if (difMax > difMin) {
            up = true;
        } else if (difMin > difMax) {
            down = true;
        }

        //подводим результаты
        if (up) {
            result = IndicatorsEnum.Up;
        } else if (down) {
            //вероятнее цена вырастет
            result = IndicatorsEnum.Down;
        } else {
            //черт его знает какой будет результат
            result = IndicatorsEnum.TerraIncognito;
        }
        return result;
    }

    public static IndicatorsEnum predicateByTender(float previousTender, float currentTender) {
        IndicatorsEnum result = null;
        boolean tenderUp = false, tenderDown = false;

        if (currentTender > previousTender)
            tenderUp = true;
        else if (currentTender < previousTender) {
            tenderDown = true;
        }

        if (tenderUp) {
            result = IndicatorsEnum.Up;
        } else if (tenderDown) {
            //вероятнее цена упадет
            result = IndicatorsEnum.Down;
        } else {
            //черт его знает какой будет результат
            result = IndicatorsEnum.TerraIncognito;
        }

        return result;
    }


}
