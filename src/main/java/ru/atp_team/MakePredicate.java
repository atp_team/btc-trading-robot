package ru.atp_team;

import ru.atp_team.PrepareData.BtceTableData;
import ru.atp_team.indicators.BtceTableIndicators;
import ru.atp_team.indicators.entry.Candle;
import ru.atp_team.indicators.IndicatorsEnum;
import ru.atp_team.libs.WorkWithDB;

import java.sql.ResultSet;
import java.util.*;

/**
 * Created by sbt-velichko-aa on 27.09.2016.
 */
public class MakePredicate {

    public void predicateVectorSchedule(int limit) throws Exception {
        //update data in table
        new BtceTableData().getDataFromBtceTable();

        //predicate by shadow and tender
        ResultSet candlesDb = WorkWithDB.executeQuery("SELECT * FROM sys.btce_table b order by b.date DESC limit "
                + (limit + 1), "db");
        TreeMap<Integer, IndicatorsEnum> shadowPredicateResult = new TreeMap<>();
        int i = 0;
        while (candlesDb.next()) {
            Candle currentCandle = new Candle(candlesDb);
            shadowPredicateResult.put(i, BtceTableIndicators.predicateByShadow(currentCandle.getDifferentMax(),
                    currentCandle.getDifferentMin()));
            i++;
        }
        shadowPredicateResult.forEach((k, v) -> System.out.println("key: " + k + ", value: " + v));
    }

    public IndicatorsEnum predicateVectorSchedule(HashMap<Integer, String[]> data) throws Exception {
        IndicatorsEnum result = null;
        IndicatorsEnum previousShadow = null;
        IndicatorsEnum currentShadow = null;
        IndicatorsEnum currentTender = null;

        //берем последние данные ORDER BY DESC
//        ResultSet rs = WorkWithDB.executeQuery("", "db");
        System.out.println("узнаем бычья свеча или медвежья");
//        rs.next();
//        long open = Long.parseLong(rs.getString("open"));
//        long close = Long.parseLong(rs.getString("close"));
//        long min = Long.parseLong(rs.getString("min"));
//        long max = Long.parseLong(rs.getString("max"));
        int time = 3;
        System.out.println("time = " + data.get(data.size() - time)[0]);
        float min = Float.parseFloat(String.valueOf(data.get(data.size() - time)[1].trim()));
        float close = Float.parseFloat(String.valueOf(data.get(data.size() - time)[2].trim()));
        float open = Float.parseFloat(String.valueOf(data.get(data.size() - time)[3].trim()));
        float max = Float.parseFloat(String.valueOf(data.get(data.size() - time)[4].trim()));
//        currentShadow = BtceTableIndicators.predicateByShadow(min, close, open, max);
        time--;
        min = Float.parseFloat(String.valueOf(data.get(data.size() - time)[1].trim()));
        close = Float.parseFloat(String.valueOf(data.get(data.size() - time)[2].trim()));
        open = Float.parseFloat(String.valueOf(data.get(data.size() - time)[3].trim()));
        max = Float.parseFloat(String.valueOf(data.get(data.size() - time)[4].trim()));
//        previousShadow = BtceTableIndicators.predicateByShadow(min, close, open, max);
        time = 3;

//        определяем объем торгов растет или нет
//        Если объем растет при дальнейшем движении цены в данном направлении, то рынок поддерживает данное движение.
//        Если объем сделок падает при дальнейшем движении цены в данном направлении, то рынок не поддерживает данное движение.
        float currentTenderFloat = Float.parseFloat(String.valueOf(data.get(data.size() - time)[5].trim()));
        float previousTenderFloat = Float.parseFloat(String.valueOf(data.get(data.size() - (time + 1))[5].trim()));
        currentTender = BtceTableIndicators.predicateByTender(previousTenderFloat, currentTenderFloat);

//        int i = 0;
//        long currentTender = Long.parseLong(rs.getString("tender"));
//        do {
//            long previousTender = Long.parseLong(rs.getString("tender"));
//            if (currentTender > previousTender)
//                tenderUp = true;
//            else if (currentTender < previousTender) {
//                tenderDown = true;
//        tenderUp = false;
//    }
//            i++;
//        } while (rs.next() && i <= 3 && tenderUp && !tenderDown);


        //подводим результаты
        if (currentShadow.equals(IndicatorsEnum.Up) && previousShadow.equals(IndicatorsEnum.Up)
                && currentTender.equals(IndicatorsEnum.Up)) {
            //веротнее цена вырастет
            result = IndicatorsEnum.Up;
        } else if (currentShadow.equals(IndicatorsEnum.Down) && previousShadow.equals(IndicatorsEnum.Down)
                && currentTender.equals(IndicatorsEnum.Down)) {
            //вероятнее цена упадет
            result = IndicatorsEnum.Down;
        } else if (currentShadow.equals(IndicatorsEnum.Up) && previousShadow.equals(IndicatorsEnum.Up)
                && currentTender.equals(IndicatorsEnum.Down)) {
            //вероятнее цена упадет
            result = IndicatorsEnum.Down;
        } else if (currentShadow.equals(IndicatorsEnum.Down) && previousShadow.equals(IndicatorsEnum.Down)
                && currentTender.equals(IndicatorsEnum.Up)) {
            //вероятнее цена упадет
            result = IndicatorsEnum.Up;
        } else if (currentShadow.equals(IndicatorsEnum.Up) && previousShadow.equals(IndicatorsEnum.Down)
                && currentTender.equals(IndicatorsEnum.Up)) {
            //веротнее цена может вырасти
            result = IndicatorsEnum.MbUp;
        } else if (currentShadow.equals(IndicatorsEnum.Down) && previousShadow.equals(IndicatorsEnum.Up)
                && currentTender.equals(IndicatorsEnum.Up)) {
            //веротнее цена может вырасти
            result = IndicatorsEnum.MbUp;
        } else if (currentShadow.equals(IndicatorsEnum.Up) && previousShadow.equals(IndicatorsEnum.Down)
                && currentTender.equals(IndicatorsEnum.Down)) {
            //веротнее цена может вырасти
            result = IndicatorsEnum.MbDown;
        } else if (currentShadow.equals(IndicatorsEnum.Down) && previousShadow.equals(IndicatorsEnum.Up)
                && currentTender.equals(IndicatorsEnum.Down)) {
            //веротнее цена может вырасти
            result = IndicatorsEnum.MbDown;
        } else {
            //черт его знает какой будет результат
            result = IndicatorsEnum.TerraIncognito;
        }
        return result;
    }

    public void execute() throws Exception {
//        HashMap<Integer, String[]> data = new BtceTableData().getDataFromBtceTable();
//        System.out.println("result = " + predicateVectorSchedule(data));
    }
}
