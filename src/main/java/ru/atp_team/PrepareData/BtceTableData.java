package ru.atp_team.PrepareData;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.atp_team.Constants;
import ru.atp_team.indicators.entry.Candle;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TreeMap;

/**
 * Created by nestor on 21.09.2016.
 */
public class BtceTableData {

    public void getDataFromBtceTable() throws Exception {
        TreeMap<String, Candle> result = new TreeMap<>();
        Document mainPage = Jsoup.connect("http://btc-e.nz/").timeout(120000)
                .userAgent("Mozilla")
                .get();
        Elements scripts = mainPage.select("script");

        Element script = scripts.stream().filter(sc -> sc.outerHtml().contains("drawVisualization")).findFirst().orElse(null);
        String temp = script.outerHtml().substring(script.outerHtml().indexOf("arrayToDataTable"),
                script.outerHtml().indexOf("true);") - 2).replace("arrayToDataTable(", "");
        temp = temp.substring(2, temp.length() - 2);
        String[] arr = temp.split("\\],\\[");

        boolean previousDay = true;
        for (String anArr : arr) {
            String[] day = anArr.split(",");
            String currentTime = day[0].replaceAll("\"", "").trim();
            if ("00:00".equals(currentTime))
                previousDay = false;
            Candle candle = new Candle(getDate(currentTime, previousDay), Float.parseFloat(day[1].trim()),
                    Float.parseFloat(day[2].trim()), Float.parseFloat(day[3].trim()), Float.parseFloat(day[4].trim()),
                    Float.parseFloat(day[5].trim()));
            candle.saveToDb();

//            result.put(getDate(currentTime, previousDay), new Candle(getDate(currentTime, previousDay), Float.parseFloat(day[1].trim()),
//                    Float.parseFloat(day[2].trim()), Float.parseFloat(day[3].trim()), Float.parseFloat(day[4].trim()),
//                    Float.parseFloat(day[5].trim())));

//            System.out.printf("Time: %s.\n" +
//                            "min: %s.\n" +
//                            "close: %s.\n" +
//                            "open: %s.\n" +
//                            "max: %s.\n" +
//                            "tender volume: %s.\n", day[0].trim(), day[1].trim(), day[2].trim(), day[3].trim(), day[4].trim(),
//                    day[5].trim());
        }
    }

    private String getDate (String time, boolean previousDay) {
        LocalDateTime dateTime = LocalDate.now().atTime(Integer.parseInt(time.split(":")[0]),
                Integer.parseInt(time.split(":")[1]));
        if (previousDay)
            dateTime = dateTime.minusDays(1);
        return dateTime.format(DateTimeFormatter.ofPattern(Constants.dateFormatter));
    }
}
